package com.mobiquity.challenge.viewmodel.main

import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.mobiquity.challenge.App
import com.mobiquity.challenge.api.product.data.model.ObservableBaseData
import com.mobiquity.challenge.api.product.data.remote.ProductRepositoryImp
import com.mobiquity.challenge.api.product.domain.model.ProductModel
import com.mobiquity.challenge.api.product.domain.model.ProductType
import com.mobiquity.challenge.api.product.domain.model.ResponseWrapper
import com.nhaarman.mockitokotlin2.doReturn
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

/**
 * Created by pcamilo on 04/01/2021.
 */
const val VALUE_ONE = 1

@RunWith(JUnit4::class)
class MainViewModelTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var repository: ProductRepositoryImp

    lateinit var viewModel: MainViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = MainViewModel(Mockito.mock(App::class.java), repository)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler{ Schedulers.trampoline()}
    }

    @Test
    fun `Test get all products SUCCESS`() {
        // Given
        Mockito.`when`(repository.getProducts()).doReturn(getObservableProductsMock(true))

        viewModel.getProducts()

        // Then
        Mockito.verify(repository, Mockito.times(VALUE_ONE)).getProducts()

        assertEquals(viewModel.products.value?.size, VALUE_ONE)
        assertEquals(viewModel.errorVisibility.get(), GONE)
    }

    @Test
    fun `Test get all products FAILUE`() {
        // Given
        Mockito.`when`(repository.getProducts()).doReturn(getObservableProductsMock(false))

        viewModel.getProducts()

        // Then
        Mockito.verify(repository, Mockito.times(VALUE_ONE)).getProducts()

        assertTrue(viewModel.products.value == null)
        assertEquals(viewModel.errorVisibility.get(), VISIBLE)
    }

    private fun getObservableProductsMock(isSuccess: Boolean) : ObservableBaseData<ArrayList<ProductModel>> {
        return ObservableBaseData.just(ResponseWrapper(isSuccess, listProductMocked()))
    }

    private fun listProductMocked(): ArrayList<ProductModel> = arrayListOf(ProductModel("1", "test product model", arrayListOf(), ProductType.ITEM_DRINK))
}