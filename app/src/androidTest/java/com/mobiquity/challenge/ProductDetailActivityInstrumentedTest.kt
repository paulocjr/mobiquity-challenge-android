package com.mobiquity.challenge

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.mobiquity.challenge.view.detail.ProductDetailActivity
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by pcamilo on 31/12/2020.
 */
@LargeTest
@RunWith(AndroidJUnit4::class)
class ProductDetailActivityInstrumentedTest {

    @Test
    fun viewEnabled() {
        ActivityScenario.launch(ProductDetailActivity::class.java)

        onView(withId(R.id.iv_logo)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
        onView(withId(R.id.contentLayout)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
        onView(withId(R.id.toolbar_layout)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))

        onView(withId(R.id.tv_name_label)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
        onView(withId(R.id.tv_name_label)).check(matches(withText(R.string.text_name_product)))

        onView(withId(R.id.tv_price_label)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
        onView(withId(R.id.tv_price_label)).check(matches(withText(R.string.text_price_product)))
    }
}