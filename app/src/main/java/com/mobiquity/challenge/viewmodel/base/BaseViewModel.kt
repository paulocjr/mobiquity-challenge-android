package com.mobiquity.challenge.viewmodel.base

import android.app.Application
import android.view.View
import android.view.View.GONE
import androidx.databinding.ObservableInt
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import com.mobiquity.challenge.api.product.domain.model.ResponseWrapper
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by pcamilo on 26/12/2020.
 */
abstract class BaseViewModel(app: Application) : AndroidViewModel(app) {

    val isLoading = ObservableInt(GONE)

    /**
     * Executes a repository request as an async task, observing its response in the main thread.
     *
     * @param Result
     * @param observable
     * @return
     */
    protected fun <Result> getAsyncData(showLoading: Boolean = true, observable: Observable<ResponseWrapper<Result>>): Observable<ResponseWrapper<Result>> {

        if (showLoading) isLoading.set(View.VISIBLE)

        return observable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnComplete {
                if (showLoading) isLoading.set(GONE)
            }
    }
}