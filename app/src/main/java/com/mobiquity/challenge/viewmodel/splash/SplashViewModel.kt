package com.mobiquity.challenge.viewmodel.splash

import android.app.Application
import com.mobiquity.challenge.viewmodel.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by pcamilo on 26/12/2020.
 */
class SplashViewModel @Inject constructor(app: Application) : BaseViewModel(app)