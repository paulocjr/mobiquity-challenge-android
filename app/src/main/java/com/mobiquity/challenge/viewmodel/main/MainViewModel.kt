package com.mobiquity.challenge.viewmodel.main

import android.app.Application
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mobiquity.challenge.api.product.data.model.onFailure
import com.mobiquity.challenge.api.product.data.model.onSuccess
import com.mobiquity.challenge.api.product.data.remote.ProductRepositoryImp
import com.mobiquity.challenge.api.product.domain.model.Item
import com.mobiquity.challenge.api.product.domain.model.ProductModel
import com.mobiquity.challenge.view.main.adapter.ProductAdapter
import com.mobiquity.challenge.viewmodel.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by pcamilo on 26/12/2020.
 */
class MainViewModel @Inject constructor(private val app: Application, private val repository: ProductRepositoryImp) : BaseViewModel(app) {

    val errorVisibility = ObservableField(GONE)

    private val _itemClicked = MutableLiveData<Triple<Item, ImageView, TextView>>()
    val itemClicked : LiveData<Triple<Item, ImageView, TextView>> =_itemClicked

    private val _products = MutableLiveData<ArrayList<ProductModel>>()
    val products: LiveData<ArrayList<ProductModel>> = _products
    val productAdapter = ProductAdapter(_itemClicked)

    fun getProducts() {
        errorVisibility.set(GONE)

        getAsyncData(observable = repository.getProducts())
            .onSuccess { response ->
                _products.value = response
            }.onFailure {
                errorVisibility.set(VISIBLE)
            }.subscribe()
    }

}