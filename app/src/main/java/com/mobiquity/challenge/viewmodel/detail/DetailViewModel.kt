package com.mobiquity.challenge.viewmodel.detail

import android.app.Application
import com.mobiquity.challenge.viewmodel.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by pcamilo on 26/12/2020.
 */
class DetailViewModel @Inject constructor(app: Application) : BaseViewModel(app) {
}