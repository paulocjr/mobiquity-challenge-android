package com.mobiquity.challenge

import android.app.Activity
import android.app.Application
import com.mobiquity.challenge.di.app.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

/**
 * Created by pcamilo on 26/12/2020.
 */
class App: Application(), HasActivityInjector {

    @Inject lateinit var activityInjector : DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        initDaggerInjection()
    }

    private fun initDaggerInjection() {
        DaggerAppComponent.builder()
                .application(this@App)
                .build()
                .inject(this@App)
    }

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector

}