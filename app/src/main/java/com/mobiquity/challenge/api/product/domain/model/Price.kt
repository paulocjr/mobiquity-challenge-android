package com.mobiquity.challenge.api.product.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by pcamilo on 27/12/2020.
 */
@Parcelize
data class Price(val amount: String, val currency: String) : Parcelable