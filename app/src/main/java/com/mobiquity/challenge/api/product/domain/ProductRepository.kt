package com.mobiquity.challenge.api.product.domain

import com.mobiquity.challenge.api.product.data.model.ObservableBaseData
import com.mobiquity.challenge.api.product.data.model.ProductResponse
import com.mobiquity.challenge.api.product.domain.model.ProductModel

/**
 * Created by pcamilo on 26/12/2020.
 */
interface ProductRepository {

    /**
     * Get all products from WebService REST
     */
    fun getProducts(): ObservableBaseData<ArrayList<ProductModel>>
}