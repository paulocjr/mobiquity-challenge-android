package com.mobiquity.challenge.api.product.data.remote

import com.mobiquity.challenge.api.product.data.model.ObservableResponse
import com.mobiquity.challenge.api.product.data.model.ProductResponse
import retrofit2.http.GET

/**
 * Created by pcamilo on 26/12/2020.
 */
interface ProductService {

    /**
     * Get all products from WebService REST
     */
    @GET(".")
    fun getProducts(): ObservableResponse<ArrayList<ProductResponse>>
}