package com.mobiquity.challenge.api.product.domain.model

import androidx.annotation.DrawableRes
import com.mobiquity.challenge.R

/**
 * Created by pcamilo on 27/12/2020.
 */
enum class ProductType(val value: String, @DrawableRes val iconResource: Int) {

    ITEM_FOOD( "Food", R.drawable.ic_food),
    ITEM_DRINK( "Drink", R.drawable.ic_drink);

    companion object {
        fun from(value: String) : ProductType {
            return values().first { it.value == value }
        }
    }
}