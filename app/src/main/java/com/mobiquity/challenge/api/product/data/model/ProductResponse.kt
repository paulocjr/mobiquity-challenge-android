package com.mobiquity.challenge.api.product.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by pcamilo on 26/12/2020.
 */
class ProductResponse(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("url") val url: String,
    @SerializedName("products") val products: List<Product>) {

    class Product (
        @SerializedName("id") val id: String,
        @SerializedName("name") val name: String,
        @SerializedName("url") val url: String,
        @SerializedName("salePrice") val salePrice: SalePrice) {

        class SalePrice(
            @SerializedName("amount") val amount: String,
            @SerializedName("currency") val currency: String)
    }

}