package com.mobiquity.challenge.api.product.data.model

/**
 * Created by pcamilo on 26/12/2020.
 */
class BaseResponse<T>(val success: Boolean, val data: T?)