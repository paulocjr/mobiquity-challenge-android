package com.mobiquity.challenge.api.product.data.remote

import com.mobiquity.challenge.api.product.data.model.ObservableBaseData
import com.mobiquity.challenge.api.product.data.model.mapToBaseData
import com.mobiquity.challenge.api.product.data.model.mapToBaseResponse
import com.mobiquity.challenge.api.product.domain.ProductRepository
import com.mobiquity.challenge.api.product.domain.model.ProductModel
import com.mobiquity.challenge.utils.productsToModel
import javax.inject.Inject

/**
 * Created by pcamilo on 26/12/2020.
 */
class ProductRepositoryImp @Inject constructor(private val service: ProductService) : ProductRepository {

    override fun getProducts(): ObservableBaseData<ArrayList<ProductModel>> =
        service.getProducts()
            .mapToBaseResponse()
            .mapToBaseData { it.productsToModel() }

}