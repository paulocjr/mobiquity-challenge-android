package com.mobiquity.challenge.api

import com.mobiquity.challenge.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by pcamilo on 26/12/2020.
 */
const val TIME_OUT = 30L
class APIClient {

    private var retrofit: Retrofit

    init {
        retrofit = Retrofit
            .Builder()
            .baseUrl(BuildConfig.APP_BASE_URL)
            .client(getOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    private fun getLoggingCapableHttpClient(): HttpLoggingInterceptor {
        val httpLogging = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) httpLogging.level = HttpLoggingInterceptor.Level.BODY
        return httpLogging
    }

    private fun getOkHttpClient() : OkHttpClient {
        return OkHttpClient()
            .newBuilder()
            .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor(getLoggingCapableHttpClient())
            .build()
    }

    fun getRetrofitInstance() = retrofit

}