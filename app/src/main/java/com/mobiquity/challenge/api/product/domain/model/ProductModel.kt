package com.mobiquity.challenge.api.product.domain.model

/**
 * Created by pcamilo on 26/12/2020.
 */
data class ProductModel(val id: String, val name: String, val products: List<Item>, val productType: ProductType)