package com.mobiquity.challenge.api.product.data.model

import com.mobiquity.challenge.api.product.domain.model.ResponseWrapper
import io.reactivex.Observable
import retrofit2.Response

/**
 * Created by pcamilo on 26/12/2020.
 */

/** Observable Response */
typealias ObservableResponse<T> = Observable<Response<T>>

/** Observable BaseResponse */
typealias ObservableBaseResponse<T> = Observable<BaseResponse<T>>

/** Observable BaseData */
typealias ObservableBaseData<T> = Observable<ResponseWrapper<T>>

/**
 * Maps the current Response object into a BaseResponse.
 *
 * @param T
 * @return
 */
fun <T> ObservableResponse<T>.mapToBaseResponse(): ObservableBaseResponse<T> {

    return flatMap { response ->

        ObservableBaseResponse.just(
            if (response.isSuccessful) {
                handleSuccessResponse(response)
            } else {
                handleErrorResponse(response)
            }
        )

    }.onErrorReturn {
        // Handle Exceptions
        BaseResponse(
            false,
            null
        )
    }
}

/**
 * Transforms the current BaseResponse object into a BaseData.
 *
 * @param T
 * @param Result
 * @param mapper
 * @return
 */
fun <T, Result> ObservableBaseResponse<T>.mapToBaseData(mapper: ((T) -> Result)): ObservableBaseData<Result> {
    return flatMap { response ->
        ObservableBaseData.just(
            ResponseWrapper(
                response.success,
                response.data?.let { mapper(it) }
            )
        )
    }
}


/**
 * Configures the ObservableSource so that it invokes an action on Success.
 *
 * @param T
 * @param onSuccess
 * @return
 */
fun <T: Any> ObservableBaseData<T>.onSuccess(
    onSuccess: ((result: T) -> Unit)
): ObservableBaseData<T> {
    return doOnNext { response ->
        handleResponse(response, true, onSuccess = { it.data?.let { data -> onSuccess(data) } })
    }
}

/**
 * Configures the ObservableSource so that it invokes an action on Error.
 *
 * @param T
 * @param onError
 * @return
 */
fun <T: Any> ObservableBaseData<T>.onFailure(
    onError: ((result: ResponseWrapper<T>) -> Unit)
): ObservableBaseData<T> {
    return doOnNext { response ->
        handleResponse(response, false, onError = { onError(it) })
    }
}

/** Helper methods */
private fun <T> handleResponse(
    response: ResponseWrapper<T>,
    requireSuccess: Boolean = true,
    onSuccess: ((ResponseWrapper<T>) -> Unit)? = null,
    onError: ((ResponseWrapper<T>) -> Unit)? = null
): ResponseWrapper<T>? {
    return when (response.success) {
        true -> {
            // Response was a success
            if (requireSuccess) {
                onSuccess?.let { it(response) }
                response
            } else { null }
        }
        else -> {
            // Response was an error
            if (!requireSuccess) {
                onError?.let { it(response) }
                response
            } else { null }
        }
    }
}

/**
 * Handles a failed HTTP response.
 *
 * @param T
 * @param response
 * @return
 */
private fun <T> handleErrorResponse(response: Response<T>): BaseResponse<T> = BaseResponse(success = false, data = response.body())

/**
 * Handles a successful HTTP response.
 *
 * @param T
 * @param response
 * @return
 */
private fun <T> handleSuccessResponse(response: Response<T>): BaseResponse<T> =BaseResponse(success = true, data = response.body())