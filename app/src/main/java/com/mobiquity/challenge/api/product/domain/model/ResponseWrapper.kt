package com.mobiquity.challenge.api.product.domain.model

/**
 * Created by pcamilo on 27/12/2020.
 */
class ResponseWrapper<T>(var success: Boolean, var data: T?)