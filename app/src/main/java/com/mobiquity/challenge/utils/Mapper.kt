package com.mobiquity.challenge.utils

import com.mobiquity.challenge.BuildConfig
import com.mobiquity.challenge.api.product.data.model.ProductResponse
import com.mobiquity.challenge.api.product.domain.model.Item
import com.mobiquity.challenge.api.product.domain.model.Price
import com.mobiquity.challenge.api.product.domain.model.ProductModel
import com.mobiquity.challenge.api.product.domain.model.ProductType

/**
 * Created by pcamilo on 27/12/2020.
 */

fun ProductResponse.toProductModel() = ProductModel(
    id,
    name,
    products = products.toItems(),
    productType = ProductType.from(name)
)

fun ProductResponse.Product.toItem() = Item(
    id,
    name,
    url = BuildConfig.APP_BASE_URL + url,
    salePrice = salePrice.toPrice()
)

fun ProductResponse.Product.SalePrice.toPrice() = Price(
    amount,
    currency
)

fun List<ProductResponse.Product>.toItems() = map { product -> product.toItem()  }

fun ArrayList<ProductResponse>.productsToModel() =
    map { item -> item.toProductModel() }.toCollection(arrayListOf())