package com.mobiquity.challenge.utils

import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mobiquity.challenge.R
import com.mobiquity.challenge.api.product.domain.model.ProductType
import com.squareup.picasso.Picasso
import java.text.NumberFormat
import java.util.*

/**
 * Created by pcamilo on 26/12/2020.
 */
object BindingAdapter {

    @JvmStatic
    @BindingAdapter("setAdapter")
    fun RecyclerView.setAdapter(rvAdapter: RecyclerView.Adapter<*>?) {
        rvAdapter?.let { adapt ->
            this.adapter = adapt
            this.layoutManager = LinearLayoutManager(this.context)
            this.addItemDecoration(DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL))
            this.adapter?.notifyDataSetChanged()
        }
    }

    @JvmStatic
    @BindingAdapter("imageUrl")
    fun ImageView.imageUrl(imageUrl: String?) {
        imageUrl?.let { img ->
            if (img.isNotEmpty()) {
                Picasso.get()
                    .load(imageUrl)
                    .error(R.drawable.ic_no_image)
                    .into(this)
            }
        }
    }

    @JvmStatic
    @BindingAdapter("setHeaderIcon")
    fun ImageView.setHeaderIcon(productType: ProductType?) {
        productType?.let { item ->
            setImageDrawable(ContextCompat.getDrawable(this.context, item.iconResource))
        }
    }

    @JvmStatic
    @BindingAdapter("price", "currency")
    fun TextView.setPriceFormatted(value: String?, currency: String?) {
        value?.let { item ->
            NumberFormat.getCurrencyInstance().apply {
                this.currency = Currency.getInstance(currency.toString())
            }.also {
                this.text = it.format(item.toFloat())
            }
        }
    }
}