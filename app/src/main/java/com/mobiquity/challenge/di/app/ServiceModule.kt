package com.mobiquity.challenge.di.app

import com.mobiquity.challenge.api.APIClient
import com.mobiquity.challenge.api.product.data.remote.ProductService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by pcamilo on 26/12/2020.
 */
@Module
class ServiceModule {

    @Provides
    @Singleton
    fun provideBitcoinService(apiClient: APIClient) = apiClient.getRetrofitInstance().create(ProductService::class.java)

}