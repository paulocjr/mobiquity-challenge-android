package com.mobiquity.challenge.di.app

import android.app.Application
import com.mobiquity.challenge.App
import com.mobiquity.challenge.di.builder.ActivityBuilder
import com.mobiquity.challenge.di.builder.ViewModelBuilder
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by pcamilo on 26/12/2020.
 */
@Singleton
@Component(
    modules = [
        AppModule::class,
        ServiceModule::class,
        ViewModelBuilder::class,
        ActivityBuilder::class,
        AndroidSupportInjectionModule::class])

interface AppComponent {

    fun inject(application: App)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}