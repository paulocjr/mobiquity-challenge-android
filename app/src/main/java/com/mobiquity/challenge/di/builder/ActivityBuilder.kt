package com.mobiquity.challenge.di.builder

import com.mobiquity.challenge.view.detail.ProductDetailActivity
import com.mobiquity.challenge.view.main.MainActivity
import com.mobiquity.challenge.view.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by pcamilo on 26/12/2020.
 */
@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun bindSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    abstract fun bindProductDetailActivity(): ProductDetailActivity

}