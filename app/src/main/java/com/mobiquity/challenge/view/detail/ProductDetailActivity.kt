package com.mobiquity.challenge.view.detail

import android.content.Context
import android.content.Intent
import android.view.MenuItem
import com.mobiquity.challenge.R
import com.mobiquity.challenge.api.product.domain.model.Item
import com.mobiquity.challenge.databinding.ActivityProductDetailBinding
import com.mobiquity.challenge.view.base.BaseActivity
import com.mobiquity.challenge.viewmodel.detail.DetailViewModel
import kotlin.reflect.KClass

const val ITEM_PRODUCT = "ITEM_PRODUCT"

class ProductDetailActivity : BaseActivity<ActivityProductDetailBinding, DetailViewModel>(R.layout.activity_product_detail) {

    override fun initActivity() = getItemShared()

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getViewModelClass(): KClass<DetailViewModel> = DetailViewModel::class

    private fun getItemShared() {
        val itemProduct = intent?.extras?.getParcelable<Item>(ITEM_PRODUCT)
        itemProduct?.let { product ->
            viewBinding.item = product
            setupToolbarTitle(product.name)
        }
    }
    
    companion object {

        fun newIntent(context: Context, item: Item): Intent {
            return Intent(context, ProductDetailActivity::class.java).apply {
                putExtra(ITEM_PRODUCT, item)
            }
        }
    }
}