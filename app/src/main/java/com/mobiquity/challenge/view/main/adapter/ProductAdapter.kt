package com.mobiquity.challenge.view.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.mobiquity.challenge.api.product.domain.model.Item
import com.mobiquity.challenge.api.product.domain.model.ProductModel
import com.mobiquity.challenge.databinding.ItemListHeaderBinding

/**
 * Created by pcamilo on 26/12/2020.
 */
class ProductAdapter(private val itemClicked : MutableLiveData<Triple<Item, ImageView, TextView>>) : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>()  {

    private var products = arrayListOf<ProductModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder = ProductViewHolder(ItemListHeaderBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    override fun getItemCount(): Int = products.size
    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) = holder.bind(products[position])

    inner class ProductViewHolder(private val binding: ItemListHeaderBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(product: ProductModel) = with(binding) {
            model = product
            itemAdapter = ItemAdapter(product.products, itemClicked)
            executePendingBindings()
        }
    }

    fun setData(items: ArrayList<ProductModel>) {
        this.products = items
        notifyDataSetChanged()
    }
}
