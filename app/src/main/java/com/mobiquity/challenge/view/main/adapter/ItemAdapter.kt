package com.mobiquity.challenge.view.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.mobiquity.challenge.api.product.domain.model.Item
import com.mobiquity.challenge.databinding.ItemProductLayoutBinding

/**
 * Created by pcamilo on 26/12/2020.
 */
class ItemAdapter(private val items: List<Item>, private val productItemClicked: MutableLiveData<Triple<Item, ImageView, TextView>>) : RecyclerView.Adapter<ItemAdapter.ProductViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder = ProductViewHolder(ItemProductLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    override fun getItemCount(): Int = items.size
    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) = holder.bind(items[position])

    inner class ProductViewHolder(private val binding: ItemProductLayoutBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Item) = with(binding) {
            model = item

            root.setOnClickListener {
                productItemClicked.postValue(Triple(item, ivLogo, tvTitle))
            }

            executePendingBindings()
        }
    }
}
