package com.mobiquity.challenge.view.splash

import android.content.Intent
import androidx.constraintlayout.motion.widget.MotionLayout
import com.mobiquity.challenge.R
import com.mobiquity.challenge.databinding.ActivitySplashBinding
import com.mobiquity.challenge.view.base.BaseActivity
import com.mobiquity.challenge.view.main.MainActivity
import com.mobiquity.challenge.viewmodel.splash.SplashViewModel
import kotlin.reflect.KClass

class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>(R.layout.activity_splash) {

    override fun initActivity() = onMotionLayoutComplete()
    override fun getViewModelClass(): KClass<SplashViewModel> = SplashViewModel::class

    private fun onMotionLayoutComplete() {
        viewBinding.contentMotionLayout.addTransitionListener(object : MotionLayout.TransitionListener {
            override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {}
            override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {}
            override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {}
            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) { openMainActivity() }
        })
    }

    private fun openMainActivity() {
        startActivity(Intent(this, MainActivity::class.java)).apply {
            finish()
        }
    }
}