package com.mobiquity.challenge.view.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.mobiquity.challenge.R
import dagger.android.AndroidInjection
import javax.inject.Inject
import kotlin.reflect.KClass

/**
 * Created by pcamilo on 26/12/2020.
 */
abstract class BaseActivity <VB : ViewDataBinding, V : ViewModel> (@LayoutRes private val layoutResId: Int) : AppCompatActivity() {

    lateinit var viewBinding: VB
    lateinit var _viewModel: V

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        _viewModel = ViewModelProviders.of(this, viewModelFactory).get(getViewModelClass().java)
        viewBinding = DataBindingUtil.setContentView<VB>(this, layoutResId).apply {
            lifecycleOwner = this@BaseActivity
            setVariable(BR.viewModel, _viewModel)
        }

        initActivity()
    }

    /**
     * Setup the toolbar with title and home back button android native
     */
    protected fun setupToolbarTitle(string: String, homeButtonEnable: Boolean = true){
        supportActionBar?.let {
            it.title = string
            it.setDisplayShowHomeEnabled(homeButtonEnable)
            it.setDisplayHomeAsUpEnabled(homeButtonEnable)
        }
    }

    protected abstract fun initActivity()
    protected abstract fun getViewModelClass(): KClass<V>
}