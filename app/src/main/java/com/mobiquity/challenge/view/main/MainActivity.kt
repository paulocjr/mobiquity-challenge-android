package com.mobiquity.challenge.view.main

import androidx.core.util.Pair
import androidx.core.app.ActivityOptionsCompat
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.mobiquity.challenge.R
import com.mobiquity.challenge.api.product.domain.model.Item
import com.mobiquity.challenge.databinding.ActivityMainBinding
import com.mobiquity.challenge.view.base.BaseActivity
import com.mobiquity.challenge.view.detail.ProductDetailActivity
import com.mobiquity.challenge.viewmodel.main.MainViewModel
import kotlin.reflect.KClass

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel> (R.layout.activity_main) {

    override fun initActivity() {
        _viewModel.getProducts()
        _viewModel.products.observe({lifecycle}) {_viewModel.productAdapter.setData(it)}
        _viewModel.itemClicked.observe({lifecycle}) { item -> openProductDetail(item) }
    }

    override fun getViewModelClass(): KClass<MainViewModel> = MainViewModel::class

    private fun openProductDetail(item: Triple<Item, ImageView, TextView>) {
        ProductDetailActivity.newIntent(this, item.first).also { intent ->
            startActivity(intent, createTransitionAnimation(item).toBundle())
        }
    }

    private fun createTransitionAnimation(item: Triple<Item, ImageView, TextView>): ActivityOptionsCompat {
        val imgAnim = Pair.create<View, String>(item.second, getString(R.string.transition_image_product))
        val titleAnim = Pair.create<View, String>(item.third, getString(R.string.transition_title_product))
        return ActivityOptionsCompat.makeSceneTransitionAnimation(this, imgAnim, titleAnim)
    }
}